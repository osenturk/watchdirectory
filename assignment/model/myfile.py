class MyFile:

    # Initializer / Instance Attributes
    def __init__(self, name, path, ctime, mtime, size, is_archived, url):
        self.name = name
        self.path = path
        self.ctime = ctime  # time of last status change
        self.mtime = mtime  # time of last modification
        self.size = size
        self.is_archived = is_archived
        self.url = url

    # setters
    def set_archive(self, is_archive):
        self.is_archived = is_archive

    def set_mtime(self, new_time):
        self.mtime = new_time

    def set_name(self, new_name):
        self.name = new_name

    def set_size(self, new_size):
        self.size = new_size

    def set_url(self, new_url):
        self.url = new_url

    def set_ctime(self, new_ctime):
        self.ctime = new_ctime

    def set_path(self, new_path):
        self.path = new_path

    # getters
    def get_archive(self):
        return self.is_archived

    def get_mtime(self):
        return self.mtime

    def get_name(self):
        return self.name

    def get_size(self):
        return self.size

    def get_url(self):
        return self.url

    def get_ctime(self):
        return self.ctime

    def get_path(self):
        return self.path

    def __repr__(self):
        return "<YourFile(name='%s', path='%s', ctime='%s', mtime='%s', size='%d', is_archived='%s, url='%s')>" % (
            self.name, self.path, self.ctime, self.mtime, self.size, self.is_archived, self.url)