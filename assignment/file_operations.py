""" File operations aggregated under this file """
import sys
import os
import yaml
import datetime
import time

from assignment.model.myfile import MyFile

cfg = None

WORKSPACE_PATH = sys.argv[1] if len(sys.argv) >= 2 else '/Users/ozansenturk/PycharmProjects/WatchDirectory/'
CONFIG_PATH = WORKSPACE_PATH + "config.yaml"

with open(CONFIG_PATH, 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)

def get_files_by_dir():

    path = cfg['monitor']['path']

    for (dirpath, dirnames, filenames) in os.walk(path):

        files = [ get_file_object(path, filename) for filename in filenames]

        return files

def get_file_object(dirpath, filename):
    stat = os.stat(os.path.join(dirpath, filename))

    t1=datetime.datetime.fromtimestamp(stat.st_ctime)
    t2=datetime.datetime.fromtimestamp(stat.st_mtime)

    myfile = MyFile(filename, dirpath,
            t1,
            t2,
           stat.st_size, False, '')

    return myfile