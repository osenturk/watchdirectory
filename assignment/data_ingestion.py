""" 1- Read the information of the files_dir in a directory
    (name, path, creation date/time, modification date/time and size)

    2- Import the data into a PostgreSQL/MySQL/MongoDB databases.
    The results should be one table or collection to store this information."""

import sys
import yaml

from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, drop_database, create_database
from sqlalchemy import Table, Column, Integer, String, MetaData, DateTime, Boolean
from sqlalchemy.sql import select, update

from assignment.file_operations import get_files_by_dir

cfg = None

WORKSPACE_PATH = sys.argv[1] if len(sys.argv) >= 2 else '/Users/ozansenturk/PycharmProjects/WatchDirectory'
CONFIG_PATH = WORKSPACE_PATH + "config.yaml"

with open(CONFIG_PATH, 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)

def get_engine():

    engine_url = cfg['psql']['engine']['url']
    engine = create_engine(engine_url)

    return engine

def initialize_database():

    create_database(cfg['psql']['engine']['url'])

    engine = get_engine()
    metadata = MetaData(engine)
    files = Table('FILES', metadata,

                  Column('id', Integer, primary_key=True),

                  Column('name', String),

                  Column('path', String),

                  Column('ctime', DateTime),

                  Column('mtime', DateTime),

                  Column('size', String),

                  Column('is_archived', Boolean),

                  Column('url', String)
                  )

    metadata.create_all(engine)


def get_connection():

    engine_url = cfg['psql']['engine']['url']
    engine = create_engine(engine_url)

    connection = engine.connect()

    return connection


def bulk_insert(dict_files):

    engine = get_engine()

    # Create MetaData instance
    meta = MetaData(engine, reflect=True)
    files_table = meta.tables['FILES']

    conn = get_connection()

    # insert multiple data
    conn.execute(files_table.insert(), dict_files)

def get_all_files():

    engine = get_engine()

    meta = MetaData(engine, reflect=True)
    table = meta.tables['FILES']

    select_st = select([table])

    conn = engine.connect()
    res = conn.execute(select_st)

    all_files = [one_file for one_file in res]

    return all_files


def get_files_by_archive(is_archived):
    engine = get_engine()
    meta = MetaData(engine, reflect=True)
    table = meta.tables['FILES']

    # select * from 'user'
    select_st = select([table]).where(
        table.c.is_archived == is_archived)

    conn = engine.connect()
    res = conn.execute(select_st)

    all_files = [one_file for one_file in res]

    return all_files

def update_file_by_name(filename):
    engine = get_engine()
    meta = MetaData(engine, reflect=True)
    table = meta.tables['FILES']

    update_stmt = update(table).where(table.c.name == filename). \
        values(is_archived=True)

    conn = engine.connect()
    conn.execute(update_stmt)


print("initializing database...")

if(database_exists(cfg['psql']['engine']['url']) == False):
    initialize_database()

files = get_files_by_dir()

dict_files = [vars(one_file) for one_file in files]

print(dict_files)

bulk_insert(dict_files)

files = get_all_files()
print(files)

archived_files = get_files_by_archive(True)
print(archived_files)
