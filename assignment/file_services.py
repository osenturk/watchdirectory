""" Develop an small web API with two endpoints to:
    get a JSON object with the available files_dir
    and get a JSON object with archived files_dir."""

#!flask/bin/python
from flask import Flask, jsonify

from assignment.data_ingestion import get_files_by_archive
from assignment.data_ingestion import get_all_files

from flask_httpauth import HTTPBasicAuth
from flask import make_response

app = Flask(__name__)

all = get_all_files()

oldies = get_files_by_archive(True)

news = get_files_by_archive(False)

auth = HTTPBasicAuth()

@auth.get_password
def get_password(username):
    if username == 'python':
        return 'pass'
    return None

@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 401)

@app.route('/api/oldies', methods=['GET'])
@auth.login_required
def get_oldies():
    return jsonify({'oldies': [dict(row) for row in oldies]})

@app.route('/api/news', methods=['GET'])
@auth.login_required
def get_news():
    return jsonify({'news': [dict(row) for row in news]})

@app.route('/api/all', methods=['GET'])
@auth.login_required
def get_all():
    return jsonify({'all': [dict(row) for row in all]})

if __name__ == '__main__':
    app.run(debug=True)