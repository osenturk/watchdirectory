""" Archive the oldest files_dir (more than 5 days).
    You should consider having a new field in the
    database table or move the files_dir to another directory."""

import os
import glob
import sys
import yaml
import time
import shutil
import schedule

from datetime import date, timedelta

from assignment.data_ingestion import update_file_by_name

cfg = None

WORKSPACE_PATH = sys.argv[1] if len(sys.argv) >= 2 else '/Users/ozansenturk/PycharmProjects/WatchDirectory/'
CONFIG_PATH = WORKSPACE_PATH + "config.yaml"

with open(CONFIG_PATH, 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)

days_old = cfg['archive']['limit']  # how old the files have to be before they are moved
original_folder = cfg['archive']['source_path']  # folder to move files from
new_folder = cfg['archive']['dest_path']  # folder to move files to
archive_freq = cfg['archive']['frequency']

# start process #
def archive_files():
    move_date = date.today() - timedelta(days=days_old)
    move_date = time.mktime(move_date.timetuple())

    print("Initialising...")

    count = 0
    size = 0.0

    for filename in glob.glob1(original_folder, "*.*"):

        srcfile = os.path.join(original_folder, filename)
        destfile = os.path.join(new_folder, filename)

        if os.stat(srcfile).st_mtime < move_date:
            if not os.path.isfile(destfile):

                size = size + (os.path.getsize(srcfile) / (1024 * 1024.0))
                shutil.move(srcfile, destfile)

                update_file_by_name(filename)
                print('table was updated for the file '+filename)
                print("Archived '" + filename + "'.")
                count = count + 1

    print("Archived " + str(count) + " files, totalling " + str(round(size, 2)) + "MB.")


# schedule.every().day.at("01:50").do(archive_files)
schedule.every(archive_freq).seconds.do(archive_files)

print("archiving is underway for ..")

index = 0
while True:
    # Checks whether a scheduled task
    # is pending to run or not
    schedule.run_pending()
    time.sleep(1)
    if (index % 10 == 0):
        print(str(index)+" seconds")

    index +=1