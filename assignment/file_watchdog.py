""" Monitor the directory to see if there are new files_dir or changes in existing ones. (You can use
    cronjobs, infinite loops with parallel threads or any other solution that you consider) """

import sys
import time
import yaml
from time import gmtime, strftime

from watchdog.observers.polling import PollingObserver
from watchdog.events import PatternMatchingEventHandler

WORKSPACE_PATH = sys.argv[1] if len(sys.argv) >= 2 else '/Users/ozansenturk/PycharmProjects/WatchDirectory/'
CONFIG_PATH = WORKSPACE_PATH + "config.yaml"

with open(CONFIG_PATH, 'r') as ymlfile:
    cfg = yaml.safe_load(ymlfile)

def on_created(event):
    print(f"{strftime('%Y-%m-%d %H:%M:%S', gmtime())} : creation = {event.src_path} ")

def on_deleted(event):
    print(f"{strftime('%Y-%m-%d %H:%M:%S', gmtime())} : deletion = {event.src_path} ")

def on_modified(event):
    print(f"{strftime('%Y-%m-%d %H:%M:%S', gmtime())} : modification = {event.src_path} ")

def on_moved(event):
    print(f"{strftime('%Y-%m-%d %H:%M:%S', gmtime())} : move = {event.src_path} ")


if __name__ == "__main__":

    patterns = cfg['monitor']['change']['patterns']
    ignore_patterns=cfg['monitor']['change']['ignore_patterns']
    ignore_directories = cfg['monitor']['change']['ignore_directories']
    case_sensitive = cfg['monitor']['change']['case_sensitive']

    my_event_handler = PatternMatchingEventHandler(patterns, ignore_patterns, ignore_directories, case_sensitive)

    my_event_handler.on_created = on_created
    my_event_handler.on_deleted = on_deleted
    my_event_handler.on_modified = on_modified
    my_event_handler.on_moved = on_moved

    observer = PollingObserver(cfg['monitor']['change']['frequency'])
    observer.schedule(my_event_handler, cfg['monitor']['path'], recursive=False)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
