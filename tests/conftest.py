""" test configuration """

import yaml
import datetime

import pytest
from pymongo import MongoClient
from sqlalchemy import create_engine
from sqlalchemy_utils import drop_database, create_database
from sqlalchemy import Table, Column, Integer, String, MetaData, DateTime, Boolean
from sqlalchemy.orm import sessionmaker

from assignment.model.myfile import MyFile


@pytest.fixture
def get_config():
    workspace = '/Users/ozansenturk/PycharmProjects/WatchDirectory/'
    config_path = workspace + 'config.yaml'
    with open(config_path, 'r') as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    return cfg


@pytest.fixture
def get_mongodb_client(request):

    client = MongoClient('mongodb://localhost:27017/')

    def fin():
        print("teardown mongodb connection")
        client.close()

    request.addfinalizer(fin)

    return client


@pytest.fixture
def get_mysql_db_url():
    return 'mysql://root:Senturk@localhost/test'


@pytest.fixture
def get_mysql_db(request,get_mysql_db_engine):

    connection = get_mysql_db_engine.connect()
    connection.execute("commit")

    def fin():
        print("teardown mysql connection")
        connection.close()

    request.addfinalizer(fin)

    return connection


@pytest.fixture
def get_postgre_db_engine(get_config):

    engine_url = get_config['psql']['engine']['url']
    engine = create_engine(engine_url)

    return engine


@pytest.fixture
def get_connection(request, get_postgre_db_engine):

    # dialect + driver: // username: password @ host:port / database
    engine = get_postgre_db_engine
    connection = engine.connect()

    def fin():
        print("teardown postgre connection")
        connection.close()

    request.addfinalizer(fin)

    return connection


@pytest.fixture
def create_table(request, get_postgre_db_engine):

    create_database(get_postgre_db_engine.url)

    metadata = MetaData()
    files = Table('FILES', metadata,

        Column('id', Integer, primary_key=True),

        Column('name', String),

        Column('path', String),

        Column('creation_time', DateTime),

        Column('modification_time', DateTime),

        Column('size', String),

        Column('is_archived', Boolean),

        Column('url', String)
    )

    metadata.create_all(get_postgre_db_engine)

    def fin():
        print("teardown postgre connection")
        drop_database(get_postgre_db_engine.url)

    request.addfinalizer(fin)

    return files


@pytest.fixture
def create_myfile_class(get_config):

    path = get_config['monitor']['path']
    size = 123
    is_archived = False
    url = 'www.ozansenturk.com'

    my_file = MyFile('abc.txt', path, datetime.datetime.now(),
                     datetime.datetime.now, size, is_archived, url)

    return my_file

@pytest.fixture
def create_yourfile_class(get_config):

    path = get_config['monitor']['path']
    size = 123
    is_archived = False
    url = 'www.ozansenturk.com'

    your_file = YourFile(name = 'abc.txt', path = path, ctime = datetime.datetime.now(),
                     mtime = datetime.datetime.now, size = size, is_archived = is_archived, url = url)

    return your_file

@pytest.fixture
def create_session(request, get_postgre_db_engine):

    create_database(get_postgre_db_engine.url)

    Session = sessionmaker(bind=get_postgre_db_engine)

    session = Session()

    def fin():
        print("teardown postgre connection")
        drop_database(get_postgre_db_engine.url)

    request.addfinalizer(fin)

    return  session
