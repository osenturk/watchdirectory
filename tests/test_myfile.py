""" test myfile.py class """


def test_initialize_class(create_myfile_class):

    my_file = create_myfile_class

    assert my_file is not None

    assert my_file.size == 123

    assert my_file.is_archived == False


def test_getters_setters(create_myfile_class):

    my_file = create_myfile_class

    my_file.set_name("ozan.txt")

    assert my_file.get_name() == "ozan.txt"

