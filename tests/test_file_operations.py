""" testing file operations """
import os


def get_files_by_dir(get_config):

    path = get_config['monitor']['path']

    filepaths = [f.path for f in os.scandir(path) if f.is_file()]

    assert len(filepaths) == 3

def test_get_attributes(get_config):

    path = get_config['monitor']['path']

    for (dirpath, dirnames, filenames) in os.walk(path):
        for filename in filenames:
            stat = os.stat(os.path.join(dirpath, filename))
            assert stat.st_ctime is not None
            assert stat.st_mtime is not None
            assert stat.st_size is not None