""" Data ingestion tests    """

import sqlalchemy
from sqlalchemy import MetaData
from sqlalchemy.sql import select, update


def test_get_engine(get_postgre_db_engine):
    engine = get_postgre_db_engine

    assert type(engine) == sqlalchemy.engine.Engine


def test_initialize_database(create_table, get_postgre_db_engine):
    files = create_table

    assert get_postgre_db_engine.dialect.has_table(get_postgre_db_engine, 'FILES') == True


def test_get_file_object(create_myfile_class):

    myfile = create_myfile_class

    assert myfile.name == 'abc.txt'

def test_bulk_insert(create_table, create_myfile_class, get_postgre_db_engine):

    my_list = []
    myfile_class1 = create_myfile_class
    myfile_class2 = create_myfile_class

    my_list.append(vars(myfile_class1))
    my_list.append(vars(myfile_class2))

    files = create_table

    conn = get_postgre_db_engine.connect()

    # insert multiple data
    conn.execute(files.insert(), my_list)

    meta = MetaData(get_postgre_db_engine, reflect=True)
    table = meta.tables['FILES']

    # select * from 'user'
    select_st = select([table]).where(
        table.c.name == 'abc.txt')
    res = conn.execute(select_st)

    assert res.rowcount == 2


def test_get_all_files(create_table, get_postgre_db_engine, create_myfile_class):

    my_list = []
    myfile_class1 = create_myfile_class
    myfile_class2 = create_myfile_class
    myfile_class3 = create_myfile_class

    my_list.append(vars(myfile_class1))
    my_list.append(vars(myfile_class2))
    my_list.append(vars(myfile_class3))

    files = create_table

    conn = get_postgre_db_engine.connect()

    # insert multiple data
    conn.execute(files.insert(), my_list)

    meta = MetaData(get_postgre_db_engine, reflect=True)
    table = meta.tables['FILES']

    select_st = select([table])

    conn = get_postgre_db_engine.connect()
    res = conn.execute(select_st)

    assert res.rowcount == 3

def test_get_files_by_archive(create_table, get_postgre_db_engine, create_myfile_class):

    my_list = []
    myfile_class1 = create_myfile_class
    myfile_class2 = create_myfile_class

    my_list.append(vars(myfile_class1))
    my_list.append(vars(myfile_class2))

    files = create_table

    conn = get_postgre_db_engine.connect()

    # insert multiple data
    conn.execute(files.insert(), my_list)

    meta = MetaData(get_postgre_db_engine, reflect=True)
    table = meta.tables['FILES']

    # select * from 'user'
    select_st = select([table]).where(
        table.c.is_archived == True)

    conn = get_postgre_db_engine.connect()
    res = conn.execute(select_st)

    assert res.rowcount == 0


def test_update_file_by_name(create_table, get_postgre_db_engine, create_myfile_class):
    my_list = []
    myfile_class1 = create_myfile_class
    myfile_class2 = create_myfile_class

    my_list.append(vars(myfile_class1))
    my_list.append(vars(myfile_class2))

    files = create_table

    conn = get_postgre_db_engine.connect()

    # insert multiple data
    conn.execute(files.insert(), my_list)

    meta = MetaData(get_postgre_db_engine, reflect=True)
    table = meta.tables['FILES']

    update_stmt = update(table).where(table.c.name == 'abc.txt'). \
        values(is_archived=True)

    conn = get_postgre_db_engine.connect()
    conn.execute(update_stmt)

    # select * from 'user'
    select_st = select([table]).where(
        table.c.is_archived == True)

    res = conn.execute(select_st)

    assert res.rowcount == 2
