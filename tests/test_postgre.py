from datetime import datetime
from sqlalchemy_utils import database_exists, drop_database, create_database

from sqlalchemy.sql import select
from sqlalchemy import update, MetaData

import pytest

def test_create_postgre_db(get_config):

    engine_url = get_config['psql']['engine']['url']

    create_database(engine_url)

    assert database_exists(engine_url) == True

    drop_database(engine_url)


def test_drop_postgre_db(get_config):

    engine_url = get_config['psql']['engine']['url']

    create_database(engine_url)

    drop_database(engine_url)

    assert database_exists(engine_url) == False


def test_create_table(get_postgre_db_engine, create_table):

    files = create_table

    assert get_postgre_db_engine.dialect.has_table(get_postgre_db_engine, 'FILES') == True


def test_insert(create_table, get_connection):

    files = create_table

    ins = files.insert().values(name='Americano.txt', path='files_dir', creation_time=datetime.now(),
                                modification_time=datetime.now(), size=1024, is_archived=True,
                                url='https://www.ozansenturk.com')

    result = get_connection.execute(ins)

    assert result.inserted_primary_key == [1]


def test_select(create_table, get_connection):

    files = create_table

    ins = files.insert().values(name='Americano.txt', path='files_dir', creation_time=datetime.now(),
                                modification_time=datetime.now(), size=1024, is_archived=True,
                                url='https://www.ozansenturk.com')

    result = get_connection.execute(ins)

    s = select([files])

    result = get_connection.execute(s)

    row = result.fetchone()

    assert row['name'] == 'Americano.txt'


def test_select_where(create_table, get_connection):

    files = create_table

    ins = files.insert().values(name='Americano.txt', path='files_dir', creation_time=datetime.now(),
                                modification_time=datetime.now(), size=1024, is_archived=True,
                                url='https://www.ozansenturk.com')

    get_connection.execute(ins)

    ins = files.insert().values(name='Latte.txt', path='abcs', creation_time=datetime.now(),
                                modification_time=datetime.now(), size=2024, is_archived=True,
                                url='https://www.ozansenturk.com')

    result = get_connection.execute(ins)

    s = select([files]).where(files.c.name == 'Latte.txt')

    result = get_connection.execute(s)

    row = result.fetchone()

    assert row['path'] == 'abcs'


def test_update(create_table, get_connection):

    files = create_table

    ins = files.insert().values(name='Americano.txt', path='files_dir', creation_time=datetime.now(),
                                modification_time=datetime.now(), size=1024, is_archived=True,
                                url='https://www.ozansenturk.com')

    result = get_connection.execute(ins)

    upd = update(files).where(files.c.name == 'Americano.txt'). \
        values(path='/tmp')
    result = get_connection.execute(upd)

    sel = select([files]).where(files.c.name == 'Americano.txt')
    result = get_connection.execute(sel)

    row = result.fetchone()

    assert row['path'] == '/tmp'


def test_schema(create_table, get_postgre_db_engine):

    files = create_table

    # Create MetaData instance
    metadata = MetaData(get_postgre_db_engine, reflect=True)

    # Get Table
    ex_table = metadata.tables['FILES']

    assert ex_table.name == files.name



