
from sqlalchemy_utils import database_exists,create_database,drop_database


def test_create_mysql_db(get_config):

    engine_url = get_config['mysql']['engine']['url']
    create_database(engine_url)

    assert database_exists(engine_url) == True

    drop_database(engine_url)


def test_drop_mysql_db(get_config):

    engine_url = get_config['mysql']['engine']['url']

    create_database(engine_url)

    drop_database(engine_url)

    assert database_exists(engine_url) == False



