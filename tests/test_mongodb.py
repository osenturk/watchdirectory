""" ‘First test then implement’,
mongodb connection tests """


import pytest

import datetime

post = {"author": "Mike", "text": "My first blog post!",
        "tags": ["mongodb", "python", "pymongo"],
        "date": datetime.datetime.utcnow()}


@pytest.fixture
def get_database(get_mongodb_client):
    db = get_mongodb_client['test_database']

    return db

@pytest.mark.skip(reason='MongoDB must be running')
def test_getting_database(get_mongodb_client):
    # db = get_connection.test_database()
    db = get_mongodb_client['test_database']

    assert db is not None

@pytest.mark.skip(reason='MongoDB must be running')
def test_getting_collection(get_database):
    collection = get_database['test-collection']

    assert collection is not None

@pytest.mark.skip(reason='MongoDB must be running')
def test_inserting_post(get_database):
    posts = get_database.posts
    posts.insert_one(post).inserted_id

    assert posts.find_one({"author": "Mike"}) is not None


