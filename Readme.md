# Justifications:

## Continuous Development, Continuous Testing

TDD is applied. First, automated test cases are created for a 
feature, then produced the minimum amount of code to pass the test, 
and finally refactored the new code to acceptable standards

PyTest is used for the test framework. Whilst it does same as 
unit test, it offers both simplicity (less boilerplate code) and 
additional features such as parameterized inputs for the functions
instead of loops

Distributed code repository bitbucket is used to support parallel 
development and collaboration.

## Persistence Layer

The environment setup is done for 3 databases, MongoDB, PostgreSQL, 
and MySQL and their initial CRUD operation tests were created. 
Structured SQL database is preferred over MongoDB due to easily 
switch between MySQL and PostgreSQL.

SQLAlchemy is used which offers the full power and flexibility of 
SQL for developers. It also offers Object Relational Mapping which 
is a well-known enterprise pattern. Additionally, it is simple and 
Pythonic.

## High Cohesive, Low Coupling

Each feature is developed into a separate module which provides 
low coupling. You can easily make changes without worrying about 
their impact on other modules in the system.

Each module does the task belongs to itself which provides high 
cohesion. It provides easy maintenance for the code.

## Object Oriented

Tables are created as Object classes to ease persistence operations
and inheritance. 


# Extras:

## Webservice Security
End points are secured with Basic HTTP authentication.
username= python and password= pass
 
## Package manager
Package manager is used to benefit from our individual packages 
between modules.

setup.py is located at the root directory which avoids several 
__init__.py creation in every package directory.

## Configuration Parser 
Entire code is dependent on configuration file so program behaviour 
can be tailored for any purpose without changing the code externally.

YAML file was (config.yaml) located at the root directory and contains 
all the configuration. It adds flexibility and easy maintenance. 
It was prefered over JSON as its ability of self-reference and 
ability of comments for each configuration.

## Additions
1- Additional the 3rd end point is also implemented for the all files
2- Schedule package was used to automate archiving based on an 
interval or on a particular time of the day

```python
import schedule

schedule.every().day.at("01:50").do()
schedule.every(12).hours.do()
```  

3- requirements.txt was created at the root directory to support
portability and containerization in the further usage.


# Roadmap:

1- Refactoring based on pylint can be applied

2- Additional tests can be applied 

3- Paging can be implemented for the end points to avoid overloads

    
# Notes:

## MongoDB

mongodb installation
    a) mkdir -p data/db
    b) mongod --dbpath data/db
    c) make sure 'waiting for connections on port 27017'

define path variables
```mysql
export PATH=$PATH:/Users/ozansenturk/downloads/mongodb-osx-x86_64-4.0.9/bin
```


## MySQL

mysql connection and creation of a newuser
  
```mysql
mysql -u root -p
    CREATE USER 'newuser'@'localhost' IDENTIFIED BY 'password';
```

```
pip install mysql-connector-python
```
define path variables
```
export PATH=$PATH:/usr/local/mysql/bin
```

```mysql
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Senturk';
```
## PostgreSQL

How to start postgresql on Ubuntu

```postgresql
pg_ctl -D /usr/local/var/postgres start
```

How to start postgresql on Mac

```postgresql
brew services start postgresql
psql postgres
```


create a new user inside the psql terminal
password must be enclosed with quotes

```postgresql
CREATE ROLE newuser WITH LOGIN PASSWORD 'password';
```

make the newuser capable of creating, editing, 
and deleting databases
```postgresql
ALTER ROLE newuser CREATEDB;
```

Quit psql terminal to be able to login using newuser
```postgresql
\q
```
creation of a user
```postgresql
psql test newuser
```


Go back to psql terminal, with `newuser` as user
```postgresql
psql postgres -U newuser
```

Create Database
```postgresql
initdb /usr/local/var/postgres
```

## Environement

How to install your custom packages

```postgresql
pip install -e .
```

###### Project structure
WatchDirectory
1. assignment # features
    - data_ingestion.py
    - file_archive.py
    - file_operations.py
    - file_services.py
    - file_watchdog.py
2. data # mongodb requires it
3. files_dir   # monitoring and archiving directory
   - archived
4. tests   # feature tests
   - conftest.py
   - test_config_file.py
   - test_data_ingestion.py
   - test_file_operations.py
   - test_mongodb.py
   - test_myfile.py
   - test_mysql.py
   - test_postgre.py
5. config.yaml
6. Readme.md
7. requirements.txt
8. setup.py